#ifndef THREAD_GUARD_H
#define THREAD_GUARD_H

#include <iostream>
#include <thread>

class ThreadGuard {
    private:
        std::thread _t ;


    public:
        explicit ThreadGuard() { } ;
        
        explicit ThreadGuard(std::thread t)  { 
             _t = std::move(t) ;
        };

        void executeThread(std::thread t) {
            _t = std::move(t) ;
        }
        
        void executeThreadIndependent(std::thread t) {
            _t = std::move(t) ;
            _t.detach() ;

        } ;

        std::thread::id get_id() {
            return _t.get_id() ;
        }

        ~ThreadGuard() {
            if(_t.joinable()) {
                std::cout << "Thread with thread id: " << _t.get_id() << " has been joined!!\n" ; 
                _t.join() ;
            }
        } 

        // Copy constructor
        // Since no copying should be allowed for a thread
        // we will make the copy constructor inactive.
        ThreadGuard(const ThreadGuard &t) = delete ;

        // Copy operator
        // Since no copying should be allowed for a thread
        // we will make the copy operator inactive.
        ThreadGuard& operator=(const ThreadGuard &t) = delete ;


} ;

#endif