#include "iostream"
#include "thread"
#include "memory"
#include "vector"
#include "mutex"
#include "future"
#include "algorithm"
#include <deque>

template <typename T> class MessageQueue {
    private:
        std::mutex _mtx ;
        std::deque<T> _messages ;
        int _numberOfMessages ;
        std::condition_variable _cond ;
        std::string _queueName ;

    public:
       
       MessageQueue(std::string name) : _queueName(name) { std::cout << this->_queueName <<" created...!" << std::endl ; }

       ~MessageQueue() { std::cout << this->_queueName <<"'s destroyed...!" << std::endl ; }

       int numOfItemsInQueue() {
            return _numberOfMessages ;
            }

        bool isDataReady() {
            // Lock the critical section
            std::lock_guard<std::mutex> lock(_mtx) ;
            return !_messages.empty() ;
        }

        void send(T &&item) {
            // Lock the critical section
            std::lock_guard<std::mutex> myLock(_mtx) ;
            
            // Print out what you are doing.
            //std::cout << "Pushing Item: " << &item << std::endl ;

            // put the vehicle instance in to the vector
            _messages.emplace_back(std::move(item)) ;

            _numberOfMessages++ ;

            // Add a delay to simulate some other work
            std::this_thread::sleep_for(std::chrono::milliseconds(100)) ;
            _cond.notify_one() ;
        }

        T recieve() {
            // Use a unique_lock 
            std::unique_lock<std::mutex> _lock(_mtx) ;

            _cond.wait( _lock , [this] { return !_messages.empty() ; } ) ; 

            // Add delay to simulate some work load
            std::this_thread::sleep_for(std::chrono::milliseconds(100)) ;

            // get the first item in the list
            auto item = _messages.back() ;

            _numberOfMessages-- ;

            // remove the retrieved item form the list
            _messages.pop_back() ;

            return item ;
        }


} ;