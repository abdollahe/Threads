#include "concurrent_queue.h"
#include "ThreadGuard.h"
#include <thread>
#include <memory>


class messageStruct {
    public:
        bool terminate ;
        std::string message ;
        messageStruct() { } ;
} ;

class WorkerClass {
    private:
        bool terminate ;
        std::string greetingMessage = "Yep I got " ;
        std::string greetingMessage2 = ", send the next one" ;
        std::shared_ptr<MessageQueue<messageStruct>> incomingQueue ;
        std::shared_ptr<MessageQueue<messageStruct>> outGoingQueue ;
        
    public:
        WorkerClass(std::shared_ptr<MessageQueue<messageStruct>> inQueue , std::shared_ptr<MessageQueue<messageStruct>> outQueue) {
            this->incomingQueue = inQueue ;
            this->outGoingQueue = outQueue ;
            terminate = false ;
            std::cout << "This is thread class 1" << std::this_thread::get_id() << std::endl ; 
        }

        ~WorkerClass() {
            std::cout << "terminating the thread class 1\n" ;
        }

        
        void infiniteLoop() {
            std::cout << "In thread class 1 infinite loop " << std::this_thread::get_id() << std::endl ;
            while(terminate == false) {
                if(incomingQueue->isDataReady()) {
                    auto data = incomingQueue->recieve() ;

                    if(data.terminate) {
                        std::cout << "Terminate command received, shutting down...\n" ;
                        this->terminate = true ;

                    }
                    else {
                        messageStruct msg ;
                        msg.terminate = false ;
                        
                        msg.message = this->greetingMessage + "\"" + data.message + "\"" +this->greetingMessage2 ;  

                        outGoingQueue->send(std::move(msg)) ;
                    }
                }    
            }
        } 
         
} ;



void threadTask(std::shared_ptr<MessageQueue<messageStruct>> inQ , std::shared_ptr<MessageQueue<messageStruct>> outQ) {
    std::cout << "in threadTask: " << std::this_thread::get_id() << std::endl ;
    // create a thread class
    WorkerClass class1(outQ , inQ) ;
    class1.infiniteLoop() ;   
} ; 

int main () {
    
    std::shared_ptr<MessageQueue<messageStruct>> incomingQueue(new MessageQueue<messageStruct>("")) ;
    std::shared_ptr<MessageQueue<messageStruct>> outGoingQueue(new MessageQueue<messageStruct>("")) ;

    ThreadGuard thread1 ;
    thread1.executeThread(std::thread(threadTask , incomingQueue , outGoingQueue)) ;

    //std::cout << "In Main: " << std::this_thread::get_id() << std::endl ;
    
    std::this_thread::sleep_for(std::chrono::milliseconds(1000)) ; 

    std::string message ; 
    while(message != "end") {
        std:: cout << "-> " ;
        getline(std::cin, message);
        
        messageStruct msg ;
        if(message == "end") {
            msg.message = "" ;
            msg.terminate = true ;
        }
        else {
            msg.message = message ;
            msg.terminate = false ;
        }
        outGoingQueue->send(std::move(msg)) ;
         
        std::this_thread::sleep_for(std::chrono::milliseconds(200)) ; 

        if(incomingQueue->isDataReady()) {
            auto data = incomingQueue->recieve() ;
            std::cout << "=> " << data.message << std::endl ;
        }  
    }

std::cout << "Terminating the program\n" ;


return 0 ;

}